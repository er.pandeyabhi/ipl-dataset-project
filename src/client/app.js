function highcharts(path,containerId,title,subtitle,yAxisText,seriesname)
{
    fetch(path)
    .then(response => response.json())
    .then(data => {
        Highcharts.chart(containerId, {

            title: {
                text: title
            },
        
            subtitle: {
                text: subtitle
            },
            xAxis: {
                categories: Object.keys(data)
            },
        
            yAxis: {
                title: {
                    text: yAxisText
                } 
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },
            series: [{name:seriesname,data:Object.values(data)}],
        });
    })

    

}

function matchesWonPerTeam() {

    fetch('../output/matchesWonPerTeam.json')
      .then(response => response.json())
      .then(matches => {
        const data = Object.entries(matches);
        const matchesWonPerYear = data.map(element => ({
          name: element[0],
          data: Object.values(element[1])
        }));
        const years = data.map(element => Object.keys(element[1]));
        console.log(years);
        Highcharts.chart('matchesWonPerTeam', {
          chart: {
            type: 'column'
          },
          title: {
            text: 'Matches Won By team Per Year'
          },
          xAxis: {
            categories: years[0]
          },
          yAxis: {
            title: {
              text: 'no. of matches'
            }
          },
          plotOptions: {
            column: {
              stacking: 'normal'
            }
          },
          series: matchesWonPerYear
        });
      });
}


function main(){
    highcharts('../output/matchesPlayedPerYear.json',
    'matchesPlayedPerYear',
    'Matches Played Per Year',
    'Source: IPL.com',
    'Number of matches',
    'matches played per year');

    highcharts('../output/extraRunsConceeded.json',
    'extraRunsConceeded',
    'Extra Runs Conceeded Per Team In 2016',
    'Source: IPL.com',
    'Extar Run',
    'Extra Runs Conceeded Per Team In 2016');

    highcharts('../output/top10EconomicalBowlers.json',
    'top10EconomicalBowlers',
    'top 10 most economic bowler of 2015',
    'Source: IPL.com',
    'economy bowler',
    'top 10 most economic bowler of 2015');

    matchesWonPerTeam();
}
  
  
  
  
  
