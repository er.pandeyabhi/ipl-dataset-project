let fs = require('fs')
let csvToJson = require('convert-csv-to-json');
let matchesDataSet=csvToJson.fieldDelimiter(',').getJsonFromCsv('../data/matches.csv');

let deliveriesDataSet=csvToJson.fieldDelimiter(',').getJsonFromCsv('../data/deliveries.csv');

let call = require('./ipl.js')
//Number of matches played per year for all the years in IPL.

let matchesPlayedPerYear = call.matchesPlayedPerYear(matchesDataSet)
var output = JSON.stringify(matchesPlayedPerYear)
fs.writeFileSync('../output/matchesPlayedPerYear.json', output, 'utf-8')


 //Calculating matches won per team per year
      
let matchesWonPerTeam = call.matchesWonPerTeam(matchesDataSet)
output = JSON.stringify(matchesWonPerTeam)
fs.writeFileSync('../output/matchesWonPerTeam.json', output, 'utf-8')


//  runs concedeExtrad per team in 2016
let extraRunsConceeded = call.extraRunsConceeded(matchesDataSet, deliveriesDataSet, 2016)
output = JSON.stringify(extraRunsConceeded)
fs.writeFileSync('../output/extraRunsConceeded.json', output, 'utf-8')

//Top 10 economical bowlers in 2015
let top10EconomicalBowlers = call.top10EconomicalBowlers(matchesDataSet, deliveriesDataSet,2015,10)
output = JSON.stringify(top10EconomicalBowlers)
fs.writeFileSync('../output/top10EconomicalBowlers.json', output, 'utf-8')

// team won the toss and also won the match

let teamWonTossAndMatch =call.teamWonTossAndMatch(matchesDataSet)
output = JSON.stringify(teamWonTossAndMatch)
fs.writeFileSync('../output/teamWonTossAndMatch.json', output, 'utf-8')

//  Find player per season who has won the highest number of Player of the Match awards

let MostValuablePlayer = call.MostValuablePlayer(matchesDataSet)
output = JSON.stringify(MostValuablePlayer)
fs.writeFileSync('../output/MostValuablePlayer.json', output, 'utf-8')

// Find the strike rate of the batsman Virat Kohli for each season

let StrikerRateOfBatsman = call.StrikerRateOfBatsman(matchesDataSet, deliveriesDataSet, 'V Kohli')
output = JSON.stringify(StrikerRateOfBatsman)
fs.writeFileSync('../output/StrikerRateOfBatsman.json', output, 'utf-8')

// Find the highest number of times one player has been dismissed by another player

let mostDismissed = call.mostDismissed(deliveriesDataSet)
output = JSON.stringify(mostDismissed)
fs.writeFileSync('../output/mostDismissed.json', output, 'utf-8')

// Find the bowler with the best economy in super overs
let superoverEconomicBowler = call.superoverEconomicBowler(deliveriesDataSet)
output = JSON.stringify(superoverEconomicBowler)
fs.writeFileSync('../output/superoverEconomicBowler.json', output, 'utf-8')