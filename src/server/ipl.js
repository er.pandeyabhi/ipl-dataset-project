 //Calculating matches played per year per team through all seasons

function matchesPlayedPerYear(matchesDataSet) {
  if (!Array.isArray(matchesDataSet) || matchesDataSet.length < 1) {
    return "Wrong input format or input data is empty"
  } else {
    return matchesDataSet.map(match => match['season'])
      .reduce((totalMatchesPerYear, season) => {
        totalMatchesPerYear[season] = (totalMatchesPerYear[season] || 0) + 1;cd
        return totalMatchesPerYear;
      }, {});
  }
}


//Calculating matches won per team per year


function matchesWonPerTeam(matchesDataSet) {
  if (!Array.isArray(matchesDataSet) || matchesDataSet.length < 1) {
    return "Wrong input format or input data is empty"
  } else {
    let yearsArr = [...new Set(matchesDataSet.map(match => match.season))].sort((a, b) => a > b);
    return matchesDataSet.reduce((winnings, match) => {
      if (match.winner !== '') {
        if (winnings[match.winner]) {
          winnings[match.winner][match.season] += 1;
        } else {
          let matches = yearsArr.reduce((yearsObject, year) => {
            yearsObject[year] = 0;
            return yearsObject;
          }, {});
          matches[match.season] = 1;
          winnings[match.winner] = matches;
        }
      }
      return winnings;
    }, {});
  }
}


//Finding match ids for the target year


function matchIdArr(matchesDataSet, year) {
  return matchesDataSet.filter(match => match.season == year).map(match => match['id']);
}


//Calculating extra runs conceeded per team in 2016


function extraRunsConceeded(matchesDataSet, deliveriesDataSet, year) {
  if (!Array.isArray(matchesDataSet) || !Array.isArray(deliveriesDataSet) || !Number.isInteger(year) || matchesDataSet.length < 1 || deliveriesDataSet.length < 1) {
    return "Wrong input format or input data is empty"
  }
  if (year < 2008 || year > 2017) {
    return "Input year is not found"
  }
  let matchIdArrForYear = matchIdArr(matchesDataSet, year);
  return deliveriesDataSet.reduce((extraRunsPerTeam, match) => {
    if (matchIdArrForYear.includes(match['match_id'])) {
      extraRunsPerTeam[match['bowling_team']] = ((extraRunsPerTeam[match['bowling_team']] || 0) + Number(match['extra_runs']));
    }
    return extraRunsPerTeam;
  }, {});
}


//Calculating top 10 most economic bowler of 2015


function top10EconomicalBowlers(matchesDataSet, deliveriesDataSet, year, numOfBowlersNeeded) {
  if (!Array.isArray(matchesDataSet) || !Array.isArray(deliveriesDataSet) || !Number.isInteger(year) || matchesDataSet.length < 1 || deliveriesDataSet.length < 1 || !Number.isInteger(numOfBowlersNeeded)) {
    return "Wrong input format or input data is empty"
  }
  if (year < 2008 || year > 2017) {
    return "Input year is not found"
  }
  let matchIdArrForYear = matchIdArr(matchesDataSet, year);
  let runsConceededPerBowler = deliveriesDataSet.reduce((runsPerBowler, perBallData) => {
    if (matchIdArrForYear.includes(perBallData.match_id)) {
      runsPerBowler[perBallData['bowler']] = ((runsPerBowler[perBallData['bowler']] || 0) + Number(perBallData['total_runs']) - Number(perBallData['bye_runs']) - Number(perBallData['legbye_runs']));
    }
    return runsPerBowler;
  }, {});
  let ballsBowledByBowler = deliveriesDataSet.reduce((ballsBowled, perBallData) => {
    if (matchIdArrForYear.includes(perBallData.match_id)) {
      if (perBallData['noball_runs'] == 0 && perBallData['wide_runs'] == 0) {
        ballsBowled[perBallData['bowler']] = (ballsBowled[perBallData['bowler']] || 0) + 1;
      }
    }
    return ballsBowled;
  }, {});
  let runsConceededPerBowlerArr = Object.entries(runsConceededPerBowler);
  let economyData = runsConceededPerBowlerArr.reduce((acc, elem) => {
    acc[elem[0]] = elem[1] / Math.ceil(ballsBowledByBowler[elem[0]] / 6);
    return acc;
  }, {});
  return Object.entries(economyData)
    .sort((a, b) => a[1] - b[1])
    .slice(0, numOfBowlersNeeded)
    .reduce((acc, curr) => {
      acc[curr[0]] = curr[1];
      return acc;
    }, {});
}

// Find the number of times each team won the toss and also won the match

function teamWonTossAndMatch(matches) {
  const winnerTossAndMatch = matches.reduce((winnersToss, teams) => {
    if (teams['winner'] !== '' && teams['winner'] == teams['toss_winner']) {
      if (!winnersToss[teams.season]) {
        //checking whether the season is already present or not
        winnersToss[teams['season']] = {}; //create object if not present
      }
      if (!winnersToss[teams.season][teams['winner']]) {
        winnersToss[teams['season']][teams['winner']] = 1;
      } else {
        winnersToss[teams['season']][teams['winner']] += 1;
      }
    }
    return winnersToss;
  }, {});
  return winnerTossAndMatch;
}


//  Find player per season who has won the highest number of Player of the Match awards

function MostValuablePlayer(matches) {
  const player = matches.reduce((player, match) => {
    if (match['player_of_match'] !== '') {
      if (!player[match.season]) {
        player[match['season']] = {};
      }
      if (!player[match.season][match['player_of_match']]) {
        player[match['season']][match['player_of_match']] = 1;
      } else {
        player[match['season']][match['player_of_match']] += 1;
      }
    }
    return player;
  }, {});
  //console.log(player);
  playerMomList = Object.entries(player).reduce((mom, data) => {
    mom[data[0]] = Object.entries(data[1]).sort((a, b) => b[1] - a[1]);
    return mom;
  }, {});
  const valuablePlayer = Object.entries(playerMomList).reduce(
    (names, player) => {
      names[player[0]] = player[1][0][0];
      return names;
    },
    {}
  );
  return valuablePlayer;
}

// calculating balls and runs conceded by each bowler in super over

function superoverEconomicBowler(deliveries) {
  
  const bowlersStats = deliveries.reduce((totalRunsAndBalls, delivery) => {
    if (delivery['is_super_over'] == '1') {
      if (!totalRunsAndBalls[delivery['bowler']]) {
        totalRunsAndBalls[delivery['bowler']] = {};
      }
      if (totalRunsAndBalls[delivery['bowler']].ballsByBowler) {
        totalRunsAndBalls[delivery['bowler']].ballsByBowler += 1;
      } else {
        totalRunsAndBalls[delivery['bowler']].ballsByBowler = 1;
      }
      if (totalRunsAndBalls[delivery['bowler']].runsConcededByBowler) {
        totalRunsAndBalls[delivery['bowler']].runsConcededByBowler += parseInt(
          delivery.total_runs
        );
      } else {
        totalRunsAndBalls[delivery.bowler].runsConcededByBowler = parseInt(
          delivery.total_runs
        );
      }
    }
    return totalRunsAndBalls;
  }, {});
  //converting key and values as an array
  const economyOfBowlers = Object.entries(bowlersStats).reduce(
    (runRate, data) => {
      runRate[data[0]] =
        data[1].runsConcededByBowler / (data[1].ballsByBowler / 6);
      return runRate;
    }, {});

  //finding top 10 economic bolwler
  const topEconomicBowlers = Object.entries(economyOfBowlers).sort(
    (a, b) => a[1] - b[1]
  );
  //console.log(topEconomicBowlers)
  return topEconomicBowlers[0];
}

// Find the strike rate of the batsman Virat Kohli for each season

function StrikerRateOfBatsman(matches, deliveries, player) {
  //calulate matchids per season
  var runsAndBallsPerMatch = deliveries.reduce((perBallruns, delivery) => {
    if (delivery['batsman'] == player) {
      if (perBallruns[delivery['match_id']]) {
        if (perBallruns[delivery['match_id']]['runs']) {
          perBallruns[delivery['match_id']]['runs'] += parseInt(
            delivery['batsman_runs']
          );
          perBallruns[delivery['match_id']]['balls']++;
        }
      } else {
        perBallruns[delivery['match_id']] = {};
        perBallruns[delivery['match_id']]['runs'] = parseInt(
          delivery['batsman_runs']
        );
        perBallruns[delivery['match_id']]['balls'] = 1;
      }
    }
    return perBallruns;
  }, {});
  //generate  matches played per season
  let totalRuns = 0;
  let totalBalls = 0;
  //grouping season wise runs and balls
  let runsAndBallsPerSeason = matches.reduce(
    (groupingSeasonWiseRunsAndBalls, matchInsideMatches) => {
      if (groupingSeasonWiseRunsAndBalls[matchInsideMatches['season']]) {
        if (runsAndBallsPerMatch.hasOwnProperty(matchInsideMatches['id'])) {
          if (
            groupingSeasonWiseRunsAndBalls[matchInsideMatches['season']]['runs']
          ) {
            totalRuns = runsAndBallsPerMatch[matchInsideMatches['id']]['runs'];
            totalBalls =
              runsAndBall
              sPerMatch[matchInsideMatches['id']]['balls'];
            groupingSeasonWiseRunsAndBalls[matchInsideMatches['season']][
              'runs'
            ] += totalRuns;
            groupingSeasonWiseRunsAndBalls[matchInsideMatches['season']][
              'balls'
            ] += totalBalls;
          } else {
            totalRuns = runsAndBallsPerMatch[matchInsideMatches['id']]['runs'];
            totalBalls =
              runsAndBallsPerMatch[matchInsideMatches['id']]['balls'];
            groupingSeasonWiseRunsAndBalls[matchInsideMatches['season']][
              'runs'
            ] = totalRuns;
            groupingSeasonWiseRunsAndBalls[matchInsideMatches['season']][
              'balls'
            ] = totalBalls;
          }
        }
      } else {
        if (runsAndBallsPerMatch.hasOwnProperty(matchInsideMatches['id'])) {
          groupingSeasonWiseRunsAndBalls[matchInsideMatches['season']] = {};
          groupingSeasonWiseRunsAndBalls[matchInsideMatches['season']]['runs'] =
            runsAndBallsPerMatch[matchInsideMatches['id']]['runs'];
          groupingSeasonWiseRunsAndBalls[matchInsideMatches['season']][
            'balls'
          ] = runsAndBallsPerMatch[matchInsideMatches['id']]['balls'];
        }
      }
      return groupingSeasonWiseRunsAndBalls;
    },
    {}
  );
  let runsAndBallsPerSeasonIntoArray = Object.entries(runsAndBallsPerSeason);
  let strikeRateOverAll = runsAndBallsPerSeasonIntoArray.reduce(
    (perSeasonEconomy, perSeasonRunsAndBalls) => {
      perSeasonEconomy[perSeasonRunsAndBalls[0]] =
        (perSeasonRunsAndBalls[1].runs / perSeasonRunsAndBalls[1].balls) * 100;
      return perSeasonEconomy;
    },
    {}
  );
  return strikeRateOverAll
}

// Find the highest number of times one player has been dismissed by another player

function mostDismissed(deliveries) {
  const dismiisedObj = deliveries.reduce((dissmissed, delivery) => {
    if (delivery['batsman'] == delivery['player_dismissed']) {
      if (!dissmissed[delivery['batsman']]) {
        dissmissed[delivery['batsman']] = {};
      }
      if (dissmissed[delivery['batsman']][delivery['bowler']]) {
        dissmissed[delivery['batsman']][delivery['bowler']] += 1;
      } else {
        dissmissed[delivery['batsman']][delivery['bowler']] = 1;
      }
    }
    return dissmissed;
  }, {});
  const dismissAsObject = Object.entries(dismiisedObj).reduce(
    (dismiss, players) => {
      dismiss[players[0]] = Object.entries(players[1]).sort(
        (a, b) => b[1] - a[1]
      );
      return dismiss;
    },
    {}
  );
  const mostDismissed = Object.entries(dismissAsObject).reduce(
    (names, player) => {
      names[player[0]] = player[1][0];
      return names;
    },
    {}
  );
  return mostDismissed;
}



module.exports = {
  matchesPlayedPerYear,
matchesWonPerTeam,
extraRunsConceeded,
top10EconomicalBowlers,
teamWonTossAndMatch,
MostValuablePlayer,
StrikerRateOfBatsman,
mostDismissed,
superoverEconomicBowler
};
